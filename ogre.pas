unit Ogre;

{$mode objfpc}{$H+}

// Public
interface

uses
  Characters;

type
  TOgre = class(TCharacter)
    public
    	constructor create; override;
	end;


// Private
implementation

constructor TOgre.create();
begin
  SetHealth(40);
  SetDefence(40);
  SetAttack(25);
end;

end.

uses
	Classes, SysUtils, Characters;
