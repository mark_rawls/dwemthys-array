unit Characters;

{$mode objfpc}{$H+}

// Public
interface

uses
	Classes, SysUtils;

type
  TCharacter = class(TOBject)
    protected
      FHealth, FAttack, FDefence: Integer; // Stat values
      procedure SetHealth(newValue: Integer); // Initial stat modifier
      procedure SetAttack(newValue: Integer); // Initial stat modifier
      procedure SetDefence(newValue: Integer); // Initial stat modifier
      function TakeDamage(damage: Integer) : Integer; // Receiver method
    public
      constructor Create(); virtual; // Overwritten in inheritors
      procedure SayShrek(); // SHREK IS LOVE
      function GetHealth(): Integer; // Getter method
      function GetAttack(): Integer; // Getter method
      function GetDefence():Integer; // Getter method
      function DoDamage(var enemy: TCharacter) : Integer; // Sender method
    //published // Deprecated due to permissions violations
      //property Health: Integer read GetHealth write SetHealth default 0; // attr_reader :health
      //property Attack: Integer read GetAttack write SetAttack default 0; // attr_reader :attack
      //property Defence: Integer read GetDefence write SetDefence default 0; // attr_reader :defence
	end;


// Private
implementation
  // Monotonous bullshit
  constructor TCharacter.Create();
  begin
    // banana banana banana
	end;
  procedure TCharacter.SayShrek;
  begin
    WriteLn('Shrek!');
	end;

  // Setter methods
  procedure TCharacter.SetHealth(newValue: Integer);
  begin
    // Add newValue onto health
    FHealth:= FHealth + newValue;
	end;
  procedure TCharacter.SetAttack(newValue: Integer);
  begin
    // Add newValue onto attack
    FAttack:= FAttack + newValue;
	end;
  procedure TCharacter.SetDefence(newValue: Integer);
  begin
    // Add newValue onto defence
    FDefence:= FDefence + newValue;
	end;

  // Getter methods
  function TCharacter.GetHealth() : Integer;
  begin
    // Set return value equal to FHealth
    GetHealth:= FHealth;
  end;
  function TCharacter.GetAttack() : Integer;
  begin
    // Set return value equal to FAttack
    GetAttack:= FAttack;
	end;
  function TCharacter.GetDefence(): Integer;
  begin
    // Set return value equal to FDefence
    GetDefence:= FDefence;
	end;

  // Interaction methods
	function TCharacter.TakeDamage(damage: Integer) : Integer;
  begin
    if ((damage - FDefence) <= 0) then // If damage is less than 0, don't heal ourselves. Asshole.
    	TakeDamage:= 0
		else // Otherwise, take the goddamn hit like a man.
    begin
    	TakeDamage:= damage - FDefence;
     	SetHealth(-(damage - FDefence));
		end;
  end;
	function TCharacter.DoDamage(var enemy: TCharacter) : Integer; // Pass enemy by reference instead of value
  begin // Call receiver method on other end
    DoDamage:= enemy.TakeDamage(random(FAttack) + 0);
    // Processing our own damage? Pfft right let their armor figure that shit out
	end;

end.
