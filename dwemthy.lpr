program dwemthy;

{$mode objfpc}{$H+}

uses
	{$IFDEF UNIX}{$IFDEF UseCThreads}
	cthreads,
	{$ENDIF}{$ENDIF}
	Classes, sysutils, Characters, Ogre;

var
  Shrek: TOgre;

begin
  Randomize();
  Shrek:= TOgre.Create();
  WriteLn(Shrek.GetHealth());
  WriteLn(Shrek.GetAttack());
  WriteLn(Shrek.GetDefence());
end.

